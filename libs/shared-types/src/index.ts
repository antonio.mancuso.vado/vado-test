export * from './lib/shared-types';

export interface Cocktail {
  name: string;
  imageUrl: string;
  instructions: string;
  ingredients: string[];
}

export interface CocktailsPaged {
  cocktails: Cocktail[];
  page: number;
  totalPages: number;
}